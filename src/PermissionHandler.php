<?php

namespace Drupal\node_type_permissions;

use Drupal\node\Entity\NodeType;
use Drupal\user\PermissionHandlerInterface as UserPermissionHandlerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides the available permissions based on yml files.
 *
 * @see filter.permissions.yml
 * @see \Drupal\filter\FilterPermissions
 * @see user_api
 */
class PermissionHandler implements PermissionHandlerInterface {

  /**
   * User permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $userPermissionHandler;

  /**
   * Route matcher.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Node type (from route parameter).
   *
   * @var Drupal\node\Entity\NodeType
   */
  protected $nodeType;

  /**
   * PermissionHandler constructor.
   *
   * @param \Drupal\user\PermissionHandlerInterface $user_permission_handler
   *   Original permission handler from the user module.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Core route matcher.
   */
  public function __construct(UserPermissionHandlerInterface $user_permission_handler, RouteMatchInterface $route_match) {
    $this->userPermissionHandler = $user_permission_handler;
    $this->routeMatch = $route_match;

    $this->setNodeTypeFromRoute();
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    return $this->filterPermissions($this->userPermissionHandler->getPermissions());
  }

  /**
   * Filter permissions.
   *
   * Keep only node permissions for the current node type.
   *
   * @param array $all_permissions
   *   Full list of permissions that need filtering.
   *
   * @return array
   *   Filtered permissions
   */
  protected function filterPermissions(array $all_permissions) {
    return array_filter(
      $all_permissions,
      function ($permission, $permission_name) {
        if ($permission['provider'] !== 'node') {
          return FALSE;
        }

        $arguments = $permission['title']->getArguments();
        if (!array_key_exists('%type_name', $arguments) || $arguments['%type_name'] !== $this->nodeType->get('name')) {
          return FALSE;
        }

        return TRUE;
      },
      ARRAY_FILTER_USE_BOTH
    );
  }

  /**
   * Get the current node type from the current route.
   */
  protected function setNodeTypeFromRoute() {
    if (!$node_type_id = $this->routeMatch->getParameter('node_type')) {
      throw new BadRequestHttpException('Missing route parameter "node_type"');
    }

    $this->nodeType = NodeType::load($node_type_id);
  }

}
