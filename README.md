# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Maintainers

# INTRODUCTION

This module will add a "permissions" tab to the edit node-type screen.  
The tab will display only the permissions for this specific node-type.  
While adding a new node-type this will help in the process of defining the 
correct rights for the newly added node-type, instead of being forced 
to go to the user-permissions page (or forgetting to).

# REQUIREMENTS
 * Drupal 8
 * Node module
 * User module

# Installation

> composer require drupal/node_type_permissions

# Maintainers

* Paul Dudink - https://www.drupal.org/u/vrijdenker
